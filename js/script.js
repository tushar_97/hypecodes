/**
 * @file
 * Theme hooks for the Drupal Bootstrap base theme.
 */
(function ($, Drupal, hypecodes, Attributes) {


    $(".search-icon").click(function(){
        $(".search-box").css("display","block");
    
      });
    
        $(".closesidebar").click(function(){
        $(".search-box").css("display","none");;
    
      });
    

      $(window).scroll(function() {
        if ($(document).scrollTop() > 70) {
          $("header").addClass("nav-up");
        } else {
          $("header").removeClass("nav-up");
        }
      });

      $(".closebutton").click(function(){
        $(".navbar-collapse").css("height","0vh");;
    
      });

})(window.jQuery, window.Drupal, window.Drupal.gizra, window.Attributes);
